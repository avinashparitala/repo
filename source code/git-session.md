
# Pre-requisites

## Diff

	Allows us to detect differences between files

	cd ~/tmp/diff-session

	Two files there.
	file_1:

```
One good line

One old line
```

	file_2:
```
One good line

One old but updated line

One new line
```
	
	Now we can see the difference with:

	diff -u file_1 file_2

## Explain concept of git, git hosting providers (bitbucket, github)

## Benefits:

* reproducible code and projects
* history (identify errors through time and revert)
* accounting of exactly who did that and when
* only review exact changes, no need to review the whole codebase to be sure of what is being deployed
* indirect: increased collaboration via more engaging code review
* clear and easy contribution process
* clear tracking of versions among different locations of the code
* See [The "Joel Test" for Data Science](https://blog.dominodatalab.com/joel-test-data-science/), the answer to the below is negative at the moment:
	* Can data scientists find and reproduce past experiments and results, using the original code, data, parameters, and software versions?
	* Does collaboration happen through a system other than email?
	* Is there a single place to search for past research and reusable data sets, code, etc?

## Demo

	With `gource` installed (`sudo apt install gource`), show example of git history in minikube

	cd ~/git && git clone https://github.com/kubernetes/minikube.git
	cd minikube && gource

## Explain concepts of git

* repo
* commit
* branch
* master or other main branches, and development branches
* github flow


==

# Git session

Site: https://bitbucket.org/nelson_batalha/health-claims-fraud
Adapt here to your own example.


1. Create repo in Bitbucket: 

	health-claims-fraud


2. Create folder locally and start repo

	mkdir ~/git/health-claims-fraud
	cd ~/git/health-claims-fraud 
	Sailaja
	avinash

	# show .git folder now created in there

3. Config local repo to add the remote repo, with a name "origin"

	git remote add origin ssh://git@bitbucket.org/nelson_batalha/health-claims-fraud.git

	# show ~/.git/config now with the remote added

4. Create README file. Add to staging on master
	
	ls
	git status
	git add README.md
	git status

5. Commit change to our local master

	git commit -am "Adding initial README"

	git log

6. Commit change to remote master

	# open website of repo, nothing is there yet

	git push 
	git push --set-upstream origin master
	git push

	# open website of repo again, now is there

7. Now I need to do some other piece of work, say we don't like the README and want to improve it.

	# first, pull updates that have since happened to remote
	git pull origin master
	git pull

	git branch feature/UAN-012-improve-README
	git status
	git branch
	git checkout feature/UAN-012-improve-README

	git checkout master
	git branch -D feature/UAN-012-improve-README

	git checkout -b feature/UAN-012-improve-README

	# do changes in README

	git status
	git diff
	# talk about how + signs mean new lines, - signs mean erased lines

	git add README.md
	git commit -m "Improve format of README and add contributors"
	git log

	# say how alternatively we could've done this in one step by
	#git commit -am "Improve format of README and add contributors"

	# show how we can still see diffs
	git diff #COMMIT #COMMIT

8. Submit to bitbucket
	# open website, show that branch isn't there

	# now I want to push to the remote bitbucket

	git push
	git push --set-upstream origin feature/UAN-012-improve-README
	git push

	# open website, show that branch is there

9. Merge Request / Pull Request

	# Now I want to submit my work
	# See diff. 
	# Create Pull Request (add nice description)
	# Make some comments
	# Approve or Add some comments :thumbsup: or LGTM

10. Wrap up
	git checkout master
	# see README.md
	git pull
	# see README.md
	git branch -D feature/UAN-012-improve-README


## Git Advanced

### Show how we should document commands in either gists or in some personal wiki or similar to keep track of the less common commands we don't need to memorize. My example:

https://bitbucket.org/nelson_batalha/notes/wiki/Git

### Show rebase example to squash commits 

git rebase -i HEAD~3

### Git revert

git revert HEAD

### Verbally discuss complex merges, that have to be manual (when users branch from the same commit, make changes on the same file) - show commit branch where this happens on the whiteboard


git reset --hard HEAD~1

### Example working history:

cd ~/git/minikube
git log --graph --oneline


# Links

* https://www.udacity.com/course/how-to-use-git-and-github--ud775
* More help, specific to github but easily adaptable to any hosting: https://help.github.com/
* Example repo (Merge requests, long history of commits, etc): https://github.com/kubernetes/kubernetes
